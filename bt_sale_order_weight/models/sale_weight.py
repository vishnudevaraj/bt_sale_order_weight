# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


from odoo import api, fields, models

class SaleOrder(models.Model):
    _inherit = "sale.order"
    
    def _compute_weight_total(self):
        for sale in self:
            weight_tot = 0
            for line in sale.order_line:
                if line.product_id:
                    weight_tot += line.weight_net or 0.0
            sale.total_weight_net = weight_tot


    total_weight_net = fields.Float(string='Total Weight (kg)', compute='_compute_weight_total')


# Record the net weight of the order line
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def _compute_weight_net(self):
        for line in self:
            weight_net = 0
            if line.product_id and line.product_id.weight:
                weight_net += (line.product_id.weight
                        * line.product_uom_qty / line.product_uom.factor)
            line.weight_net = weight_net


    weight_net = fields.Float(string='Net Weight (kg)', compute='_compute_weight_net')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
